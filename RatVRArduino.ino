int ledPin = 20; // LEDが接続されているピン番号
int buttonPin = 1; // ボタンが接続されているピン番号
bool ledState = false; // LEDの状態を管理
bool buttonState = false; // ボタンの状態を管理
unsigned long previousClockMillis = 0; // クロック信号の送信時間を記録
const unsigned long clockInterval = 5; // クロック信号の送信間隔（ミリ秒）
bool clockSignal = false; // 現在のクロック信号状態（0または1）

void setup() {
  pinMode(ledPin, OUTPUT); // LEDのピンを出力モードに設定
  pinMode(buttonPin, INPUT_PULLUP); // ボタンのピンを入力モードに設定
  Serial.begin(115200); // シリアル通信を9600bpsで開始
}

void loop() {
  // クロック信号を送信
  unsigned long currentMillis = millis();
  if (currentMillis - previousClockMillis >= clockInterval) {
    previousClockMillis = currentMillis;
    clockSignal = !clockSignal; // クロック信号を反転

    //ボタンの状態も一緒に送信する（押されている場合は'1'、押されていない場合は'0'）
    
    
    buttonState = (digitalRead(buttonPin) == HIGH); 
    Serial.print(clockSignal ? '1' : '0');
    Serial.print(buttonState ? '1' : '0'); // クロック信号を送信
    Serial.println();  // 改行を追加
  }
  
  
  if (Serial.available() > 0) {
    char command = Serial.read(); // シリアルからデータを読み込む
    switch (command) {
      case 'P':
      case 'F':
        digitalWrite(ledPin, HIGH); // LEDを点灯
        ledState = true;
        break;
      case 'L':
        blink(200); // 早い点滅（200ミリ秒間隔）
        break;
      case 'R':
        blink(500); // 遅い点滅（500ミリ秒間隔）
        break;
      case 'N':
        digitalWrite(ledPin, LOW); // LEDを消灯
        ledState = false;
        break;
    }
  }

  // ボタンが押されたかどうかを確認し、押された場合は'P'をUnityに送信
  if (digitalRead(buttonPin) == HIGH) {
    if (!ledState) {
      digitalWrite(ledPin, HIGH); // ボタンが押されたときにLEDを点灯
      
      ledState = true;
    }
  } else {
    if (ledState) {
      digitalWrite(ledPin, LOW); // ボタンが押されていないときにLEDを消灯
      
      ledState = false;
    }
  }
}

void blink(int interval) {
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    digitalWrite(ledPin, !digitalRead(ledPin)); // LEDの状態を反転
    previousMillis = currentMillis; // 次のインターバルのために前回の時間を更新
  }
}
