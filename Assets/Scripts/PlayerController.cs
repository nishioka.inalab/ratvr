using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f; // プレイヤーの移動速度
    public float rotateSpeed = 100f; // プレイヤーの回転速度
    public bool rotateMode = false;

    void Update()
    {
        // マウスの移動量を取得
        float moveHorizontal = Input.GetAxis("Mouse X"); // 横移動
        float moveVertical = Input.GetAxis("Mouse Y");   // 縦移動

        if (rotateMode)
        {
            // プレイヤーの移動方向を計算
            Vector3 movement = new Vector3(0f, 0f, moveVertical);

            // プレイヤーの位置を更新
            transform.Translate(movement * moveSpeed * Time.deltaTime);

            // 横移動を回転に変換
            float rotation = moveHorizontal * rotateSpeed * Time.deltaTime;

            // プレイヤーを回転させる
            transform.Rotate(0f, rotation, 0f);
        }
        else
        {
            // プレイヤーの移動方向を計算
            Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

            // プレイヤーの位置を更新
            transform.Translate(movement * moveSpeed * Time.deltaTime);
        }
        
    }
}
