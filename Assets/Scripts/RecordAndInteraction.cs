﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class RecordAndInteraction : MonoBehaviour
{
    public GameObject targetObject;
    public Transform respawnPoint;
    public string filePath, metaFilePath;
    private List<string> dataLines;

    SerialPort serialPort;
    public float raycastDistance = 0.11f;
    public float backSpeed = 2f; // 移動速度
    public float respawnRange = 3.0f;
    public LayerMask rewardLayer,wallLayer;
    private bool wallDetected = false;
    private bool pushAction = false;
    Thread serialThread;
    string receivedData = "";

    bool currentClockSignal,lastClockSignal = false;
    DateTime lastClockTime;

    void Start()
    {
        serialPort = new SerialPort("COM3", 115200); // Arduinoとの通信に使用するシリアルポートを設定
        serialPort.Open(); // シリアルポートを開く

        // 固定時間ステップを5msに設定
        Time.fixedDeltaTime = 0.005f; // 5ms（200Hz）
        // 保存先のフォルダを設定
        string directoryPath = "Assets/Data";
        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath); // フォルダが存在しない場合は作成
        }

        // 日時付きのファイル名を作成
        string timeStamp = DateTime.Now.ToString("yyyy_MMdd_HHmmss");
        filePath = Path.Combine(directoryPath, $"data_{timeStamp}.csv");
        metaFilePath = Path.Combine(directoryPath, $"meta_{timeStamp}.csv");

        MetaDataField.root_dir = filePath;

        dataLines = new List<string>();
        dataLines.Add("Time,X,Y,Z,SIGNAL,REWARD,BUTTON");

        // メタデータを記録
        SaveMetaData();

        StartCoroutine(SignalCheck());
        StartCoroutine(RecordPosition());
        
    }

    void FixedUpdate()
    {
        RaycastHit hitForward;
        RaycastHit hitLeft;
        RaycastHit hitRight;


        //UnityEngine.Debug.LogFormat($"FixedUpdate : {1.0f / Time.deltaTime}fps, {Time.deltaTime * 1000.0f}ms");
        wallDetected = false;

        if (Input.GetKey(KeyCode.Space))
        {
            SendSignalToArduino('P');
            UnityEngine.Debug.Log("PushButton");
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (Physics.Raycast(targetObject.transform.position, targetObject.transform.forward, out hitForward, raycastDistance, rewardLayer))
        {
            SendSignalToArduino('F');
            UnityEngine.Debug.Log("Hit Forward");
            wallDetected = true;
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (!wallDetected && Physics.Raycast(targetObject.transform.position, -targetObject.transform.right, out hitLeft, raycastDistance, rewardLayer))
        {
            SendSignalToArduino('L');
            UnityEngine.Debug.Log("Hit Left");
            wallDetected = true;
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (!wallDetected && Physics.Raycast(targetObject.transform.position, targetObject.transform.right, out hitRight, raycastDistance, rewardLayer))
        {
            SendSignalToArduino('R');
            UnityEngine.Debug.Log("Hit Right");
            wallDetected = true;
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (Physics.Raycast(targetObject.transform.position, targetObject.transform.forward, out hitForward, raycastDistance, wallLayer))
        {
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (Physics.Raycast(targetObject.transform.position, -targetObject.transform.right, out hitLeft, raycastDistance, wallLayer))
        {
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (Physics.Raycast(targetObject.transform.position, targetObject.transform.right, out hitRight, raycastDistance, wallLayer))
        {
            // 2秒後にテレポートを実行
            StartCoroutine(TeleportAfterDelay(2f));
        }

        if (!wallDetected && !Input.GetKey(KeyCode.Space))
        {
            SendSignalToArduino('N'); // どの方向にも壁が検知されない場合に 'N' を送信
        }

        // 受信したデータをメインスレッドで処理する
        if (!string.IsNullOrEmpty(receivedData))
        {

            // 受信データがクロック信号とボタンの状態を持っていると仮定
            if (receivedData.Length == 2)
            {
                // クロック信号を処理
                currentClockSignal = receivedData[0] == '1';
                pushAction = receivedData[1] == '1'; // ボタンの押下状態を取得

                lastClockSignal = currentClockSignal;
                receivedData = ""; // データをクリア
            }

        }
    }

    void SaveMetaData()
    {
        List<string> metaLines = new List<string>
        {
            "Key,Value",
            $"root_dir,{MetaDataField.root_dir}",
            $"program_name,{MetaDataField.program_name}",
            $"project,{MetaDataField.project}",
            $"rig,{MetaDataField.rig}",
            $"experimenter,{MetaDataField.experimenter}",
            $"experimant_name,{MetaDataField.experimant_name}",
            $"discription,{MetaDataField.discription}",
            $"subject,{MetaDataField.subject}",
            $"body_weight,{MetaDataField.body_weight}",
            $"temperture,{MetaDataField.temperture}",
            $"humidity,{MetaDataField.humidity}"
        };

        File.WriteAllLines(metaFilePath, metaLines);
        UnityEngine.Debug.Log($"Meta data saved to: {metaFilePath}");
    }

    public void SendUIButtonPush()
    {
        SendSignalToArduino('P');
    }

    void SendSignalToArduino(char signal)
    {
        if (serialPort != null && serialPort.IsOpen)
        {
            serialPort.Write(signal.ToString()); // Arduinoに信号を送信
        }
    }

    void OnDestroy()
    {
        // スレッドを停止してシリアルポートを閉じる
        if (serialThread != null && serialThread.IsAlive)
        {
            serialThread.Join();
        }

        if (serialPort != null && serialPort.IsOpen)
        {
            serialPort.Close();
        }
    }

    //Unity上で確認するためのもの
    IEnumerator SignalCheck()
    {
        WaitForFixedUpdate wait = new WaitForFixedUpdate();
        while (true) 
        {
            try
            {
                string data = serialPort.ReadLine();

                UnityEngine.Debug.Log("Raw received data: " + data);  // 実際に受信したデータをログ出力
                
                lock (receivedData)
                {
                    receivedData = data;
                }

                UnityEngine.Debug.Log("Received data: " + receivedData);  // 実際に受信したデータをログ出力
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogWarning("Error reading serial data: " + e.Message);
            }
            yield return wait;
        }
    }

    IEnumerator RecordPosition()
    {
        WaitForFixedUpdate wait = new WaitForFixedUpdate();

        while (true)
        {
            float currentTime = Time.time;
            Vector3 position = targetObject.transform.position;

            string line = $"{currentTime},{position.x},{position.y},{position.z},{currentClockSignal},{wallDetected},{pushAction}";
            dataLines.Add(line);

            yield return wait;
        }
    }

    //元の位置に戻す処理
    private IEnumerator TeleportAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        // ランダムな位置を計算
        Vector3 randomOffset = new Vector3(
            UnityEngine.Random.Range(-respawnRange, respawnRange),  // X軸方向のランダム範囲（-1m 〜 1m）
            0,                          // 高さは変えない
            UnityEngine.Random.Range(-respawnRange, respawnRange)   // Z軸方向のランダム範囲（-1m 〜 1m）
        );

        Vector3 randomDestination = respawnPoint.position + randomOffset;

        // テレポート処理
        if (respawnPoint != null)
        {
            // 移動を開始
            yield return StartCoroutine(SmoothMove(randomDestination));
        }
        else
        {
            UnityEngine.Debug.LogWarning("Teleport point is not set.");
        }
    }

    private IEnumerator SmoothMove(Vector3 destination)
    {
        Vector3 startPosition = targetObject.transform.position;
        float distance = Vector3.Distance(startPosition, destination);
        float elapsedTime = 0f;

        while (elapsedTime < distance / backSpeed)
        {
            // 線形補間で位置を更新
            targetObject.transform.position = Vector3.Lerp(startPosition, destination, elapsedTime / (distance / backSpeed));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        // 最後の位置を正確に設定
        targetObject.transform.position = destination;
    }

    void OnApplicationQuit()
    {
        if (serialPort != null && serialPort.IsOpen)
        {
            serialPort.Close(); // シリアルポートを閉じる
        }

        if (serialThread != null && serialThread.IsAlive)
        {
            serialThread.Join(); // スレッドが終了するまで待機
        }

        UnityEngine.Debug.Log("filePath =" + MetaDataField.root_dir);
        
        // CSVにデータを書き出す
        File.WriteAllLines(MetaDataField.root_dir, dataLines);
    }
}
