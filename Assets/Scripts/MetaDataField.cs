using System.Collections;
using System.Collections.Generic;

//メタデータ記録用
[System.Serializable]
public class MetaDataField
{
    //自動入力
    public static string root_dir;

    //事前設定
    public static string program_name      = "ratvr";   
    public static string project           = "Autobiographical memory";
    public static string rig               = "Rig2";

    public static string experimenter      = "Shoya Sugimoto";
    public static string experimant_name   = "Training";
    public static string description       = "1st training";
    

    //GUIでの入力部分
    public static string subject;
    public static float body_weight;
    public static float temperture;
    public static float humidity;

}
