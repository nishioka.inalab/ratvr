using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLock : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // カーソルを中央に固定し、見えないようにする
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

}
