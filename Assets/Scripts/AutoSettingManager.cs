using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Diagnostics;

public class AutoSettingManager : MonoBehaviour
{
    [SerializeField]
    TMP_InputField subjectInput;
    [SerializeField]
    TMP_InputField tempertureInput;
    [SerializeField]
    TMP_InputField bodyWeightInput;
    [SerializeField]
    TMP_InputField humidityInput;

    [SerializeField]
    TMP_Text errorMessage; // エラー表示用のテキスト

    public void OnClickStart()
    {
        // 入力チェック
        if (string.IsNullOrWhiteSpace(subjectInput.text))
        {
            ShowError("Subject field is required.");
            return;
        }
        if (string.IsNullOrWhiteSpace(tempertureInput.text) || !float.TryParse(tempertureInput.text, out _))
        {
            ShowError("Temperature field must be a valid number.");
            return;
        }
        if (string.IsNullOrWhiteSpace(bodyWeightInput.text) || !float.TryParse(bodyWeightInput.text, out _))
        {
            ShowError("Body weight field must be a valid number.");
            return;
        }
        if (string.IsNullOrWhiteSpace(humidityInput.text) || !float.TryParse(humidityInput.text, out _))
        {
            ShowError("Humidity field must be a valid number.");
            return;
        }

        // データを設定
        MetaDataField.subject = subjectInput.text;
        MetaDataField.temperture = float.Parse(tempertureInput.text);
        MetaDataField.body_weight = float.Parse(bodyWeightInput.text);
        MetaDataField.humidity = float.Parse(humidityInput.text);

        // シーン遷移
        SceneManager.LoadScene(1);
    }

    private void ShowError(string message)
    {
        if (errorMessage != null)
        {
            errorMessage.text = message; // UIのエラー表示
        }
        UnityEngine.Debug.LogError(message); // コンソールログにエラーを出力
    }
}
