int ledPin = 20; // LEDが接続されているピン番号
int buttonPin = 1; // ボタンが接続されているピン番号
bool ledState = false; // LEDの状態を管理
bool buttonState = false; // ボタンの状態を管理
unsigned long previousClockMillis = 0; // クロック信号の送信時間を記録
const unsigned long clockInterval = 5; // クロック信号の送信間隔（ミリ秒）
bool clockSignal = false; // 現在のクロック信号状態（0または1）

bool blinkActive = false; // 点滅処理がアクティブかどうか
int blinkCycles = 0; // 現在の点滅回数
int totalBlinkCycles = 0; // 設定した点滅回数
unsigned long blinkOnTime = 0; // LEDのON時間
unsigned long blinkOffTime = 0; // LEDのOFF時間
bool lastCommandWasBlink = false; // 直前に受信したコマンドが F/L/R だったかを管理

void setup() {
  pinMode(ledPin, OUTPUT); // LEDのピンを出力モードに設定
  pinMode(buttonPin, INPUT_PULLUP); // ボタンのピンを入力モードに設定
  Serial.begin(115200); // シリアル通信を115200bpsで開始
}

void loop() {
  // クロック信号を送信
  unsigned long currentMillis = millis();
  if (currentMillis - previousClockMillis >= clockInterval) {
    previousClockMillis = currentMillis;
    clockSignal = !clockSignal; // クロック信号を反転

    // ボタンの状態も一緒に送信する（押されている場合は'1'、押されていない場合は'0'）
    buttonState = (digitalRead(buttonPin) == HIGH);
    Serial.print(clockSignal ? '1' : '0');
    Serial.print(buttonState ? '1' : '0');
    Serial.print(ledState ? '1' : '0');
    Serial.println();  // 改行を追加
  }

  // 点滅処理の実行
  if (blinkActive) {
    blink();
  }


  // ボタンが押されたとき、F/L/R の命令がない場合のみ点滅
  if (!blinkActive && digitalRead(buttonPin) == HIGH) {
    startBlink(300, 600, 3);
  }


  if (Serial.available() > 0) {
    char command = Serial.read(); // シリアルからデータを読み込む
    
    if(!blinkActive){
      switch (command) {
        case 'P':
        case 'F':
        case 'L':
        case 'R':
          startBlink(300, 600, 3); // 300ms点灯、600ms消灯の周期で3回点滅
          break;
      }
    }
    
  }
}



// 点滅処理
void blink() {
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();

  // 点滅が終わったら処理をしない
  if (blinkCycles >= totalBlinkCycles * 2) {
    blinkActive = false;
    digitalWrite(ledPin, LOW); // 点滅終了後はLEDをOFF
    return;
  }

  // LEDのON/OFFを管理
  if (ledState && (currentMillis - previousMillis >= blinkOnTime)) {
    digitalWrite(ledPin, LOW); // LEDをOFF
    ledState = false;
    previousMillis = currentMillis;
    blinkCycles++;
  } 
  else if (!ledState && (currentMillis - previousMillis >= blinkOffTime)) {
    digitalWrite(ledPin, HIGH); // LEDをON
    ledState = true;
    previousMillis = currentMillis;
    blinkCycles++;
  }
}

// 点滅を開始する関数（外部から呼び出す）
void startBlink(unsigned long onTime, unsigned long offTime, int totalCycle) {
  blinkActive = true;
  blinkCycles = 0;
  totalBlinkCycles = totalCycle;
  blinkOnTime = onTime;
  blinkOffTime = offTime;
}
